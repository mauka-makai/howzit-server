package com.example.howzit.messages;


public class PublishMessage extends Message {
    //attributes are final. no fields of a new received msg should be changed
    private final int senderID;    //ID of the sender, to be iserted in the Server DB
    private String senderIP;     //IP of the sender, to be iserted in the Server DB

    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public PublishMessage(int senderID, String senderIP){
        super(1); // 1 is code for PUBLISH message
        this.senderID = senderID;
        this.senderIP = senderIP;
    }

    public int getSenderID() {return this.senderID;}
    public String getSenderIP() {return this.senderIP;}
    public void setSenderIP(String ip) { 
    	this.senderIP = ip;
    }
}
