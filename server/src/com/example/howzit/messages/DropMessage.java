package com.example.howzit.messages;

public class DropMessage extends Message{
    //attributes are final. no fields of a new received msg should be changed
    private final String dropID;    //ID for which to delete saved IP on the server

    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public DropMessage(String dropID){
        super(4); // 6  is code for DROP message
        this.dropID = dropID;
    }

    public String getDropID() {return this.dropID;}

}
