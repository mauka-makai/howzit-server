package com.example.howzit.messages;

public class SignUpMessage extends Message {
    //attributes are final. no fields of a new received msg should be changed
    private final String key;    //public key to send to the Server


    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public SignUpMessage(String key){
        super(4); // 4 is code for SIGN_UP message
       this.key = key;
    }

    public String getKey() {return this.key;}
}
