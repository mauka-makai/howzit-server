package com.example.howzit.messages;

public class TextMessage extends Message{

    //attributes are final. no fields of a new received msg should be changed
    private final int receiverID;  //ID of the receiver, for redundancy
    private final int senderID;    //ID of the sender, to choose the right key for signature decryption
    private final String text;        //text of the message
    private final String signature;   //same as text, but encrypted.


    //all the fields are filled up in the costructor so that we are sure no messages are sent with empty fields.
    public TextMessage(int receiverID, int senderID, String text){
        super(0); // 0 is code for TEXT message
        this.receiverID = receiverID;
        this.senderID = senderID;
        this.text = text;
        //later on, signature will be placed in the Message class, with abstract method Sign()
        this.signature = this.Sign();
    }

    public int getReceiverID() {return this.receiverID;}
    public int getSenderID() {return this.senderID;}
    public String getText() {return this.text;}
    public String getSignature() {return this.signature;}


    public String Sign(){
        //TO DO
        String encryptedText = "abc";
        return encryptedText;
    }

}


