package com.example.howzit.messages;

public class QueryReplyMessage extends Message {
    //attributes are final. no fields of a new received msg should be changed
    private final int requestedID;    //ID of the user whom IP has been requested
    private final String requestedIP;   //IP of the requested user

    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public QueryReplyMessage(int requestedID, String requestedIP){
        super(3); // 3 is code for QUERY_REPLY message
        this.requestedID = requestedID;
        this.requestedIP = requestedIP;

    }

    public int getRequestedID() {return this.requestedID;}
    public String getRequestedIP() {return this.requestedIP;}
}
