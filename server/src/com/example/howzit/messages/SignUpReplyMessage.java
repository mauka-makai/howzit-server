package com.example.howzit.messages;

public class SignUpReplyMessage extends Message {

    //attributes are final. no fields of a new received msg should be changed
    private final String newID;    //new generated ID


    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public SignUpReplyMessage(String newID){
        super(5); // 5 is code for SIGN_UP_REPLY message
        this.newID = newID;
    }

    public String getNewID() {return this.newID;}
}
