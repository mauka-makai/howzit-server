package com.example.howzit.messages;

public class QueryMessage extends Message {
    //attributes are final. no fields of a new received msg should be changed
    private final int requestedID;    //ID of the user whom we want the IP

    //all the fields are filled up in the costructor so that we are sure no message is sent with empty fields
    public QueryMessage(int requestedID){
        super(2); // 2 is code for QUERY message
        this.requestedID = requestedID;

    }

    public int getRequestedID() {return this.requestedID;}
}
