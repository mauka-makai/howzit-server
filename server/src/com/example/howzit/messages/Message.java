package com.example.howzit.messages;

import java.io.Serializable;

//Implementing serializable we can send an object of this class through sockets
public abstract class Message implements Serializable {
    /*private static final int TEXT = 0;
    private static final int PUBLISH = 1;
    private static final int QUERY = 2;
    private static final int QUERY_REPLAY = 3;*/

    protected final int type;


    public Message(int type){
        this.type = type;
    }

    public int getType(){ return this.type; }

    //this will return the encryption of the text, or of other fields, depending on the kind of message
   // public abstract String Sign();
}



