package serverMain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.JSONWriter;

import com.example.howzit.messages.*;


/**
 * <p>This class acts as the discovery server for the Howzit application.</p>
 * 
 * <p>The server is single threaded. Therefore, all of its logic is contained in this class.
 * The server uses the JSON file format to store data in a file.</p>
 * 
 * @author Alessandro, Matteo
 * @author Nickolas
 *
 */

//prova
public class ServerMain implements Runnable{

	//Nick: Only use relative paths to the project root. 
	static String projectAbsolutePath = new File("").getAbsolutePath();
	//Nick: Paths should work under both under Linux "/" and Windows "\\" -> File.separator
	static String databaseFilePath = projectAbsolutePath + File.separator + "res" + File.separator + "database.json";

	/**
	 * HashMap containing the pairs &lt;ID, address&gt; of the users.
	 */
	private static HashMap<String, String> addressBook;
	/**
	 * Hardcoded port on which the server is listening. TODO: move it in a config file.
	 */
	public static final int LISTENING_PORT = 4444;//TODO move in a config file
	/**
	 * Hardcoded variable to properly handle JSON. I really don't know why it is there
	 */
	public static final Charset charset = Charset.forName("US-ASCII");

	// socket for client connection in current thread
	private Socket clientSocket;

	// This is called "constructor" for those who haven't seen one :)
	ServerMain(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	/**
	 * This method reads from file json the content of the internal address book
	 * 
	 * @param filePath Name of the file JSON of the database
	 * 
	 * @return true if the reading succeeded, false if an error occurred
	 */
	public static boolean readDatabase(String filePath) {

		System.out.println("Reading database " + filePath + "...");
		try {
			BufferedReader databaseIn = new BufferedReader(new FileReader(filePath));
			JSONTokener jsont = new JSONTokener(databaseIn);
			JSONObject jsono = new JSONObject(jsont);
			Set<String> keys = jsono.keySet();
			for(String key : keys) {
				addressBook.put(key, jsono.getString(key));
			}
			System.out.println("Database read!");
			databaseIn.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("Error reading database");
		return false;

	}


	/**
	 * <p>This method saves the content of the internal address book to a file.</p>
	 * <p>A boolean value is returned indicating success or failure of the operation</p>
	 * 
	 * @param filePath Path of the file JSON of the database
	 * 
	 * @return true if the writing succeeded, false if an error occurred
	 */
	public synchronized boolean writeDatabase(String filePath) {
		System.out.println("Writing database to " + filePath + "...");
		try {
			BufferedWriter databaseOut = new BufferedWriter(new FileWriter(filePath));
			JSONWriter jsonw = new JSONWriter(databaseOut);
			jsonw.object();
			Set<Map.Entry<String, String>> addressSet = addressBook.entrySet();
			for(Map.Entry<String, String> entry : addressSet) {
				jsonw.key(entry.getKey()).value(entry.getValue());
			}
			jsonw.endObject();
			System.out.println("Database wrote");
			databaseOut.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error during database writing");
			return false;
		} 
	}
	
	private static SSLServerSocketFactory createFactory()
			throws NoSuchAlgorithmException, KeyStoreException,
			CertificateException, IOException, UnrecoverableKeyException,
			KeyManagementException {
		char[] storePassword = System.getProperty("javax.net.ssl.keyStorePassword")
				.toCharArray();
		String keyPassword = "M@uka-Maka1_$ecret";
		//System.out.println(password); 
		System.out.println(System.getProperty("javax.net.ssl.keyStoreName"));

		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(new FileInputStream(System.getProperty("javax.net.ssl.keyStoreName")), storePassword);

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		System.out.println("KeyManagerFactory Algorithm: "+ kmf.getAlgorithm());
		
		kmf.init(ks, keyPassword.toCharArray());

		SSLContext context = SSLContext.getInstance("TLS");
		System.out.println("SSLContext PROTOCOL:" + context.getProtocol());
		context.init(kmf.getKeyManagers(), null, null);

		return context.getServerSocketFactory();
	}

	/**
	 * Main method. Right now, it creates the server and the socket connection. It performs the exchange of object between the client
	 * @param args Not used right now
	 * @throws CertificateException 
	 * @throws KeyStoreException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 * @throws UnrecoverableKeyException 
	 */
	public static void main(String[] args) throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {

		/**
		 * Hardcoded filepath of the database JSON file. TODO: move it in a config file
		 */


		System.out.println("Starting server...");		
		addressBook = new HashMap<String, String>();
		readDatabase(databaseFilePath);

		for (String s : addressBook.keySet()){
			System.out.println("User: "+ s + ":" + addressBook.get(s));
		}
		
		//System.setProperty("javax.net.debug", "ssl");
		System.setProperty("javax.net.ssl.keyStorePassword", "M@uka-Maka1_$ecret");
		System.setProperty("javax.net.ssl.keyStoreName", projectAbsolutePath + File.separator +
				"res"+ File.separator +"Keystore.jks");
		

		System.out.println("ServerSocket creating...");
		ServerSocket listener;
		
		String bindIP = System.getProperty("ip");
		
		try {
			if (bindIP == null) {
				//listener = new ServerSocket(LISTENING_PORT,20);
				listener = (SSLServerSocket) createFactory().createServerSocket(LISTENING_PORT);
			}
			else {
				InetSocketAddress endpoint = new InetSocketAddress(bindIP, LISTENING_PORT);
				listener = (SSLServerSocket) createFactory().createServerSocket();
				listener.bind(endpoint);
			}
			//listener.setNeedClientAuth(true);

			System.out.println("ServerSocket created");

			System.out.println("Server Ready to accept connections...");
		

			while (true) {
				//Accepting incoming connection request

				Socket sock = listener.accept();

				System.out.println("Connection accepted from " + sock.getInetAddress());
				new Thread(new ServerMain(sock)).start();   //start a new thread for the client, 
				//so that we could accept other clients on ServerSocket listener at the same time   
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	@Override
	public void run() {

		//Setting up Stream to read the Object on Socket				
		ObjectOutputStream output = null;

		try {
			output = new ObjectOutputStream(clientSocket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ObjectInputStream input = null;
		try {
			input = new ObjectInputStream(clientSocket.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Now server can send and receive
		System.out.println("Streams created!");

		Message instring = null;
		while(true) {
			try {
				instring = (Message) input.readObject();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Client probably closed the connection. Or sent some crap?");
				return;
			}

			//Message m = new PublishMessage("bho", "ciao");

			//If message received is of type 0 : CLOSE
			if(instring.getType()==0) {
				System.out.println("Message type 0 received.");
				try {
					clientSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Closed connection");
				break;

			}
			//If message received is of type 1 :PUBLISH
			else if(instring.getType()==1)  {
				System.out.println("Message type 1 received.");
				//Create PublishMessage read from the Stream
				PublishMessage msg=(PublishMessage) instring;

				//add the contact in the hashmap and then write in the database
				addressBook.put(((Integer)msg.getSenderID()).toString(), msg.getSenderIP());
				writeDatabase(databaseFilePath);
				System.out.println(msg.getSenderID()+":"+msg.getSenderIP()+" arrived");

				//Create a SimpleMessage
				TextMessage reply=new TextMessage(0, 0, "Hello "+msg.getSenderID()+" your Ip is saved in the DB\n");

				try {
					output.writeObject(reply);
					output.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  	
			}
			//If message received is of type 2 :QUERY
			else if(instring.getType()==2)  {
				System.out.println("Message type 2 received.");
				//Create QueryMessage read from the Stream
				QueryMessage msg=(QueryMessage) instring;
				System.out.print("Query message received");

				//Search Ip in the hashmap
				String requestedID = ((Integer)msg.getRequestedID()).toString();
				String ip=addressBook.get(requestedID);

				System.out.println("Requested id: " + msg.getRequestedID());
				System.out.println(ip);

				//Create a QueryReplyMessage
				QueryReplyMessage reply= new QueryReplyMessage(msg.getRequestedID(),ip);

				try {
					output.writeObject(reply);
					System.out.print("QueryReply sent.");
					output.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 	

			}
			
			else if(instring.getType()==4)  {
				System.out.println("Message type 4 (SignUp) received.");
				//Create PublishMessage read from the Stream
				SignUpMessage msg=(SignUpMessage) instring;

		        //generate random ID for the user, from 0 to 10000. next step is to ask the discovery server
		        double random = Math.random() * 9999;
		        int ID = (int)random;
		        String clientID = ((Integer)ID).toString();
		        
				//add the contact in the hashmap and then write in the database
				addressBook.put(clientID, clientSocket.getInetAddress().getHostAddress());
				writeDatabase(databaseFilePath);
				System.out.println(clientID+":"+clientSocket.getInetAddress().getHostAddress()+" arrived");

				//Create a SimpleMessage
				//TextMessage reply=new TextMessage(0, 0, "Hello "+ clientID +" your Ip is saved in the DB\n");
				SignUpReplyMessage reply= new SignUpReplyMessage(clientID);

				try {
					output.writeObject(reply);
					output.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  	
			}

		}
	}
}