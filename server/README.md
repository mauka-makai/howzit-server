# Server
This is the server for the Howzit app, written in Java. It stores the addresses of the user using a key as ID.
Use JavaDoc to get the documentation.

# How to Use

**The discovery server is platform independent.** It was tested on Linux and Windows.

First create an executable .jar file form the source code.

1. To run on Linux, please place the .jar executable, "res" folder and start-server.sh in /opt/server-executable/. Run the **start-server.sh** script.

2. To run on Windows, extract in a folder of your choosing and execute the server-runnable.jar in a similar way (change the directory paths only).

## Dependency

This version uses the package [org.json (JSON-java)](https://github.com/stleary/JSON-java), available online with license [MIT modified](https://en.wikipedia.org/wiki/Douglas_Crockford). We need to check if it is compatible with our, which is LGPL v.2.1.


## Useful files

1. Resources needed to start the program:
   - Keystore.jks containing the server's x509 certificate & private key 
   - database.json for the registered users' info
   - json library needed to run the code

2. A directory with network packet dumps from the app:

   - A sample of the actual network packets exchanged by Howzit app & the discorevy server - secured TLSv1.3 (howzit-dump.pcap)
   - A screenshot of the howzit.pcap, opened with Wireshark network analyzer 


