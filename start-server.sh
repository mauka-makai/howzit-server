#!/bin/bash

#Option 1 - production environmemt
/usr/bin/java -Duser.dir="/opt/server-executable" -Dip="10.8.0.1" -jar /opt/server-executable/server-runnable.jar >> /var/log/howzit/discovery-server.log 2>&1

#Option 2 - development / staging environment
#To display secure TLS/SSL session debug information, run the server with these parameters instead, uncomment this line and comment out Opt1:
#/usr/bin/java -Duser.dir="/opt/server-executable" -Djavax.net.debug=ssl -Dip="10.8.0.1" -jar /opt/server-executable/server-runnable.jar >> /var/log/howzit/discovery-server.log 2>&1
